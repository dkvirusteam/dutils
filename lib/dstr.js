"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isNumberStr = exports.uuid = exports.geneRandomStr = exports.ellipsis = exports.replace = void 0;
function replace(content, params, mode) {
    if (mode === void 0) { mode = '{{}}'; }
    var regex = /(\{\{\s*(\w*)\s*\}\})/g;
    if (mode === '{}')
        regex = /(\{\s*(\w*)\s*\})/g;
    while (regex.exec(content)) {
        content = content.replace(new RegExp(RegExp.$1, 'g'), params[RegExp.$2]);
    }
    return content;
}
exports.replace = replace;
function ellipsis(content, length) {
    if (length === void 0) { length = 20; }
    if (content.length > length) {
        return content.substr(0, length) + '...';
    }
    else {
        return content;
    }
}
exports.ellipsis = ellipsis;
function geneRandomStr(length, characters) {
    if (length === void 0) { length = 6; }
    if (characters === void 0) { characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'; }
    var modes = {
        '[a-z]': 'abcdefghijklmnopqrstuvwxyz',
        '[A-Z]': 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        '[0-9]': '0123456789',
    };
    Object.keys(modes).forEach(function (item) {
        characters = characters.replace(item, modes[item]);
    });
    var characterArr = characters.split('');
    var result = '';
    while (result.length < length) {
        result += characterArr[Math.floor(Math.random() * characterArr.length)];
    }
    return result;
}
exports.geneRandomStr = geneRandomStr;
function uuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}
exports.uuid = uuid;
function isNumberStr(value) {
    return Number(value).toString() !== 'NaN';
}
exports.isNumberStr = isNumberStr;
exports.default = {
    replace: replace,
    ellipsis: ellipsis,
    geneRandomStr: geneRandomStr,
    uuid: uuid,
    isNumberStr: isNumberStr,
};
