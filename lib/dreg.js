"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isUrl = exports.isPhone = exports.isEmail = void 0;
var phoneRegex = /^1[3456789]\d{9}$/;
var emailRegex = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/;
var test = function (regex, value) {
    return regex.test(value);
};
function isEmail(email) {
    return test(emailRegex, email);
}
exports.isEmail = isEmail;
function isPhone(phone) {
    return test(phoneRegex, phone);
}
exports.isPhone = isPhone;
function isUrl(url) {
    return /^https?:\/\//.test(url) || /^localhost?:/.test(url) || /^127.0.0.1?:/.test(url);
}
exports.isUrl = isUrl;
exports.default = {
    isEmail: isEmail,
    isPhone: isPhone,
    isUrl: isUrl,
};
