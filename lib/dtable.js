"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.addIndexColumn = void 0;
function addIndexColumn(tableData, current, size) {
    if (current === void 0) { current = 1; }
    if (size === void 0) { size = 10; }
    if (Object.prototype.toString.call(tableData) !== "[object Array]") {
        throw new Error("参数类型不正确");
    }
    var startIndex = (Number(current) - 1) * Number(size) + 1;
    return tableData.map(function (item, index) {
        item.index = startIndex + index;
        return item;
    });
}
exports.addIndexColumn = addIndexColumn;
exports.default = {
    addIndexColumn: addIndexColumn,
};
