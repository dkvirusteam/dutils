"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isEmptyObject = exports.isObject = exports.removeEmptyField = void 0;
var darr_1 = require("./darr");
function removeEmptyField(obj, mode) {
    if (!isObject(obj)) {
        return obj;
    }
    var emptyType = [];
    if (!mode) {
        emptyType = ['{}', '[]', 'null', 'undefined'];
    }
    else {
        emptyType = darr_1.cross(mode, ['{}', '[]', 'null', 'undefined', '', '0', 'false']);
    }
    function remove(value) {
        Object.keys(value).forEach(function (key) {
            if ((emptyType.includes('null') && obj[key] === null) ||
                (emptyType.includes('undefined') && obj[key] === undefined) ||
                (emptyType.includes('[]') && Array.isArray(obj[key]) && obj[key].length === 0) ||
                (emptyType.includes('') && obj[key] === '') ||
                (emptyType.includes('0') && obj[key] === 0) ||
                (emptyType.includes('false') && obj[key] === false) ||
                (emptyType.includes('{}') && isObject(obj[key]) && Object.keys(obj[key]).length === 0)) {
                delete obj[key];
            }
            if (isObject(obj[key])) {
                var result = removeEmptyField(obj[key], mode);
                if (emptyType.includes('{}') && Object.keys(result).length === 0) {
                    delete obj[key];
                }
            }
        });
        return value;
    }
    return remove(obj);
}
exports.removeEmptyField = removeEmptyField;
function isObject(value) {
    return Object.prototype.toString.call(value) === '[object Object]';
}
exports.isObject = isObject;
function isEmptyObject(value) {
    return isObject(value) && Object.keys(value).length === 0;
}
exports.isEmptyObject = isEmptyObject;
exports.default = {
    removeEmptyField: removeEmptyField,
    isObject: isObject,
    isEmptyObject: isEmptyObject,
};
