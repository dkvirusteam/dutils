"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.isFloat = exports.isInt = exports.isNumber = exports.formatNumberByKilo = exports.formatNumberByComma = exports.formatByte = exports.divide = exports.multiply = exports.minus = exports.plus = void 0;
function plus(arg1, arg2) {
    var r1, r2, m;
    try {
        r1 = String(arg1).split('.')[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = String(arg2).split('.')[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    m = Math.pow(10, Math.max(r1, r2));
    return (arg1 * m + arg2 * m) / m;
}
exports.plus = plus;
function minus(arg1, arg2) {
    var r1, r2, m, n;
    try {
        r1 = String(arg1).split('.')[1].length;
    }
    catch (e) {
        r1 = 0;
    }
    try {
        r2 = String(arg2).split('.')[1].length;
    }
    catch (e) {
        r2 = 0;
    }
    n = Math.max(r1, r2);
    m = Math.pow(10, n);
    return Number(((arg1 * m - arg2 * m) / m).toFixed(n));
}
exports.minus = minus;
function multiply(arg1, arg2) {
    var m = 0, s1 = String(arg1), s2 = String(arg2);
    try {
        m += s1.split('.')[1].length;
    }
    catch (e) { }
    try {
        m += s2.split('.')[1].length;
    }
    catch (e) { }
    return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m);
}
exports.multiply = multiply;
function divide(arg1, arg2) {
    var t1, t2, r1, r2;
    try {
        t1 = String(arg1).split('.')[1].length;
    }
    catch (e) {
        t1 = 0;
    }
    try {
        t2 = String(arg2).split('.')[1].length;
    }
    catch (e) {
        t2 = 0;
    }
    r1 = Number(String(arg1).replace('.', ''));
    r2 = Number(String(arg2).replace('.', ''));
    return (r1 / r2) * Math.pow(10, t2 - t1);
}
exports.divide = divide;
function formatByte(byteSize, decimal) {
    if (decimal === void 0) { decimal = 2; }
    byteSize = Number(byteSize);
    if (String(byteSize) === 'NaN')
        return '';
    if (0 == byteSize)
        return '0B';
    var times = 1024;
    var unitArr = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var index = Math.floor(Math.log(byteSize) / Math.log(times));
    return parseFloat((byteSize / Math.pow(times, index)).toFixed(decimal)) + unitArr[index];
}
exports.formatByte = formatByte;
function formatNumberByComma(value) {
    var num = Number(value);
    if (typeof value !== 'string' && typeof value !== 'number') {
        return '';
    }
    else if (String(num) === 'NaN') {
        return String(value);
    }
    else if (isInt(num)) {
        return String(num).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
    }
    else {
        var numArr = String(value).split('.');
        numArr[0] = String(numArr[0]).replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        return numArr.join('.');
    }
}
exports.formatNumberByComma = formatNumberByComma;
function formatNumberByKilo(value, opt) {
    if (typeof value !== 'string' && typeof value !== 'number') {
        return '';
    }
    var num = Number(value);
    if (String(num) === 'NaN' || num < 1000) {
        return String(value);
    }
    else {
        var unit = (opt === null || opt === void 0 ? void 0 : opt.lowerCase) === true ? 'k' : 'K';
        var newNum = (opt === null || opt === void 0 ? void 0 : opt.decimal) ? (num / 1000).toFixed(opt === null || opt === void 0 ? void 0 : opt.decimal) : (num / 1000);
        return newNum + unit;
    }
}
exports.formatNumberByKilo = formatNumberByKilo;
function isNumber(value) {
    return typeof value === 'number';
}
exports.isNumber = isNumber;
function isInt(value) {
    return typeof value === 'number' && !String(value).includes('.');
}
exports.isInt = isInt;
function isFloat(value) {
    return typeof value === 'number' && String(value).includes('.');
}
exports.isFloat = isFloat;
exports.default = {
    plus: plus,
    minus: minus,
    multiply: multiply,
    divide: divide,
    formatByte: formatByte,
    formatNumberByComma: formatNumberByComma,
    formatNumberByKilo: formatNumberByKilo,
    isNumber: isNumber,
    isInt: isInt,
    isFloat: isFloat,
};
