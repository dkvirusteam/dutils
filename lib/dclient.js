"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.hasCookie = exports.removeCookie = exports.setCookie = exports.getCookie = exports.toggleFullscreen = exports.isFullscreen = exports.exitFullscreen = exports.fullscreen = exports.getSessionStorage = exports.setSessionStorage = exports.getLocalStorage = exports.setLocalStorage = exports.downloadFile = exports.getUrlParams = exports.parseUrlParams = void 0;
function parseUrlParams(params) {
    if (typeof params === 'undefined')
        return {};
    var obj = {};
    params.split('&').forEach(function (item) {
        var arr = item.split('=');
        if (arr.length === 2) {
            obj[arr[0]] = arr[1];
        }
    });
    return obj;
}
exports.parseUrlParams = parseUrlParams;
function getUrlParams() {
    if (typeof window === 'undefined')
        return {};
    var urlParams = window.location.href.split('?')[1];
    return parseUrlParams(urlParams);
}
exports.getUrlParams = getUrlParams;
function downloadFile(url, filename, cb) {
    if (typeof window === 'undefined') {
        throw new Error('请在浏览器环境中使用该方法');
    }
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'blob';
    xhr.onload = function () {
        if (xhr.status === 200) {
            var link = document.createElement('a');
            var body = document.querySelector('body');
            link.href = window.URL.createObjectURL(xhr.response);
            link.download = filename;
            link.style.display = "none";
            body.appendChild(link);
            link.click();
            body.removeChild(link);
            window.URL.revokeObjectURL(link.href);
            cb === null || cb === void 0 ? void 0 : cb();
        }
    };
    xhr.send();
}
exports.downloadFile = downloadFile;
function setLocalStorage(key, value) {
    if (Object.prototype.toString.call(value) === "[object Array]" ||
        Object.prototype.toString.call(value) === "[object Object]") {
        value = JSON.stringify(value);
    }
    localStorage.setItem(key, value);
}
exports.setLocalStorage = setLocalStorage;
function getLocalStorage(key, defaultValue) {
    var value = localStorage.getItem(key) || '';
    if (value === 'true')
        return true;
    if (value === 'false')
        return false;
    var result;
    try {
        result = JSON.parse(value);
    }
    catch (e) {
        result = value;
    }
    if (typeof result === 'number') {
        result = String(result);
    }
    if (defaultValue && Object.prototype.toString.call(result) !== Object.prototype.toString.call(defaultValue)) {
        return defaultValue;
    }
    else {
        return result;
    }
}
exports.getLocalStorage = getLocalStorage;
function setSessionStorage(key, value) {
    if (Object.prototype.toString.call(value) === "[object Array]" ||
        Object.prototype.toString.call(value) === "[object Object]") {
        value = JSON.stringify(value);
    }
    sessionStorage.setItem(key, value);
}
exports.setSessionStorage = setSessionStorage;
function getSessionStorage(key, defaultValue) {
    var value = sessionStorage.getItem(key) || '';
    if (value === 'true')
        return true;
    if (value === 'false')
        return false;
    var result;
    try {
        result = JSON.parse(value);
    }
    catch (e) {
        result = value;
    }
    if (typeof result === 'number') {
        result = String(result);
    }
    if (defaultValue && Object.prototype.toString.call(result) !== Object.prototype.toString.call(defaultValue)) {
        return defaultValue;
    }
    else {
        return result;
    }
}
exports.getSessionStorage = getSessionStorage;
function fullscreen() {
    var element = document.documentElement;
    if (element.requestFullscreen) {
        return element.requestFullscreen();
    }
    else if (element.msRequestFullscreen) {
        return element.msRequestFullscreen();
    }
    else if (element.mozRequestFullScreen) {
        return element.mozRequestFullScreen();
    }
    else if (element.webkitRequestFullscreen) {
        return element.webkitRequestFullscreen();
    }
    else {
        return Promise.reject('浏览器未找到全屏 Api');
    }
}
exports.fullscreen = fullscreen;
function exitFullscreen() {
    if (document.exitFullscreen) {
        return document.exitFullscreen();
    }
    else if (document.msExitFullscreen) {
        return document.msExitFullscreen();
    }
    else if (document.mozCancelFullScreen) {
        return document.mozCancelFullScreen();
    }
    else if (document.webkitExitFullscreen) {
        return document.webkitExitFullscreen();
    }
    else {
        return Promise.reject('浏览器未找到全屏 Api');
    }
}
exports.exitFullscreen = exitFullscreen;
function isFullscreen() {
    return Boolean(document.fullscreenElement ||
        document.msFullscreenElement ||
        document.mozFullScreenElement ||
        document.webkitFullscreenElement) || false;
}
exports.isFullscreen = isFullscreen;
function toggleFullscreen() {
    if (isFullscreen()) {
        exitFullscreen();
    }
    else {
        fullscreen();
    }
}
exports.toggleFullscreen = toggleFullscreen;
function getCookie(key) {
    return decodeURIComponent(document.cookie.replace(new RegExp("(?:(?:^|.*;)\\s*" + encodeURIComponent(key).replace(/[-.+*]/g, "\\$&") + "\\s*\\=\\s*([^;]*).*$)|^.*$"), "$1"));
}
exports.getCookie = getCookie;
function setCookie(key, value, expires, path, secure) {
    if (expires === void 0) { expires = 3600; }
    if (path === void 0) { path = '/'; }
    if (secure === void 0) { secure = false; }
    if (!key || /^(?:expires|max\-age|path|domain|secure)$/i.test(key)) {
        return false;
    }
    var expiresTime = new Date();
    expiresTime.setTime(expiresTime.getTime() + expires * 1000);
    var sExpires = '; expires=' + expiresTime.toUTCString();
    var domain = window.location.hostname;
    document.cookie = encodeURIComponent(key) + '=' + encodeURIComponent(value) + sExpires + (domain ? '; domain=' + domain : '; path=') + (path ? '; path=' + path : '') + (secure ? '; secure' : '');
    return true;
}
exports.setCookie = setCookie;
function removeCookie(key, path, domain) {
    if (path === void 0) { path = '/'; }
    if (domain === void 0) { domain = window.location.hostname; }
    if (!key || !hasCookie(key)) {
        return false;
    }
    document.cookie = encodeURIComponent(key) + '=; expires=Thu, 01 Jan 1970 00:00:00 GMT' + (domain ? '; domain=' + domain : '') + (path ? '; path=' + path : '');
    return true;
}
exports.removeCookie = removeCookie;
function hasCookie(key) {
    return (new RegExp('(?:^|;\\s*)' + encodeURIComponent(key).replace(/[-.+*]/g, '\\$&') + '\\s*\\=')).test(document.cookie);
}
exports.hasCookie = hasCookie;
exports.default = {
    parseUrlParams: parseUrlParams,
    getUrlParams: getUrlParams,
    downloadFile: downloadFile,
    setLocalStorage: setLocalStorage,
    getLocalStorage: getLocalStorage,
    setSessionStorage: setSessionStorage,
    getSessionStorage: getSessionStorage,
    fullscreen: fullscreen,
    exitFullscreen: exitFullscreen,
    isFullscreen: isFullscreen,
    toggleFullscreen: toggleFullscreen,
    getCookie: getCookie,
    setCookie: setCookie,
    removeCookie: removeCookie,
    hasCookie: hasCookie,
};
