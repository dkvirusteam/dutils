"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getModalTitle = void 0;
function getModalTitle(operType, moduleName) {
    var operTypeTexts = {
        create: '新建',
        modify: '编辑',
        check: '查看',
    };
    return "" + (operTypeTexts[operType] || '') + (moduleName || '');
}
exports.getModalTitle = getModalTitle;
exports.default = {
    getModalTitle: getModalTitle,
};
