"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __spreadArray = (this && this.__spreadArray) || function (to, from) {
    for (var i = 0, il = from.length, j = to.length; i < il; i++, j++)
        to[j] = from[i];
    return to;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.last = exports.mergeObjArr = exports.camelCase = exports.recursion = exports.unique = exports.diff = exports.crossPlus = exports.cross = void 0;
function cross(arr1, arr2, field) {
    if (!field)
        return arr1.filter(function (item) { return arr2.includes(item); });
    return arr1.filter(function (item) { return item; }).filter(function (item) {
        if (typeof item !== 'object') {
            return arr2.includes(item);
        }
        else {
            return !!arr2.find(function (item2) { return item[field] === item2[field]; });
        }
    });
}
exports.cross = cross;
function crossPlus(twoDimensionalArr, field) {
    if (twoDimensionalArr === void 0) { twoDimensionalArr = []; }
    if (twoDimensionalArr.length === 0)
        return [];
    var result = twoDimensionalArr.reduce(function (prev, curr) {
        return cross(prev, curr, field);
    }, twoDimensionalArr[0]);
    return result;
}
exports.crossPlus = crossPlus;
function diff(arr1, arr2) {
    var diff = [];
    arr1 = __spreadArray([], arr1);
    arr2 = __spreadArray([], arr2);
    arr1.forEach(function (val1, i) {
        if (arr2.indexOf(val1) < 0) {
            diff.push(val1);
        }
        else {
            arr2.splice(arr2.indexOf(val1), 1);
        }
    });
    return diff.concat(arr2);
}
exports.diff = diff;
function unique(arr, fields) {
    if (!fields || (fields === null || fields === void 0 ? void 0 : fields.length) === 0) {
        return Array.from(new Set(arr));
    }
    var result = [];
    var obj = {};
    arr.forEach(function (item) {
        if (Object.prototype.toString.call(item) === '[object Object]') {
            var key = fields.map(function (field) { return String(item[field]); }).join('-');
            if (!obj[key]) {
                result.push(item);
                obj[key] = item;
            }
        }
        else {
            result.push(item);
        }
    });
    return Array.from(new Set(result));
}
exports.unique = unique;
function recursion(data, fieldsOrcb, mode) {
    if (mode === void 0) { mode = 'append'; }
    var iterator = function (arr, cb) {
        for (var i = 0; i < arr.length; i++) {
            cb.call(arr[i], arr[i]);
        }
    };
    var changeItem = function (item) {
        if (typeof fieldsOrcb === 'function') {
            fieldsOrcb(item);
        }
        else if (Object.prototype.toString.call(fieldsOrcb) === '[object Object]') {
            Object.keys(fieldsOrcb).forEach(function (key) {
                item[key] = item[fieldsOrcb[key]];
            });
            if (mode === 'new') {
                var arr1 = Object.keys(item);
                var arr2_1 = Object.keys(fieldsOrcb);
                var arr3 = arr1.filter(function (a1) {
                    return !arr2_1.includes(a1) && a1 !== 'children';
                });
                arr3.forEach(function (a3) { return delete item[a3]; });
            }
        }
        if (item.children) {
            iterator(item.children, changeItem);
        }
    };
    iterator(data, changeItem);
    return __spreadArray([], data);
}
exports.recursion = recursion;
function camelCase(arr, mode) {
    if (mode === void 0) { mode = 'lower'; }
    if (arr.length === 0)
        return '';
    arr = arr.map(function (item, index) {
        if (index === 0 && mode === 'lower') {
            return item.charAt(0).toLowerCase() + item.slice(1);
        }
        return item.charAt(0).toUpperCase() + item.slice(1);
    });
    return arr.join('');
}
exports.camelCase = camelCase;
function mergeObjArr(arr1, arr2, fieldsArrOrObj) {
    if (arr1 === void 0) { arr1 = []; }
    if (arr2 === void 0) { arr2 = []; }
    var getKey = function (item, arrType) {
        var key = '';
        if (Array.isArray(fieldsArrOrObj)) {
            fieldsArrOrObj.forEach(function (item2) {
                key += item[item2];
            });
        }
        else if (Object.prototype.toString.call(fieldsArrOrObj) === '[object Object]') {
            if (arrType === 'arr1') {
                Object.keys(fieldsArrOrObj).forEach(function (item2) {
                    key += item[item2];
                });
            }
            else {
                Object.values(fieldsArrOrObj).forEach(function (item2) {
                    key += item[item2];
                });
            }
        }
        return key;
    };
    var obj = {};
    arr2.forEach(function (item) {
        var key = getKey(item, 'arr2');
        obj[key] = item;
    });
    return arr1.map(function (item) {
        var key = getKey(item, 'arr1');
        if (key && obj[key]) {
            return __assign(__assign({}, item), obj[key]);
        }
        else {
            return __assign({}, item);
        }
    });
}
exports.mergeObjArr = mergeObjArr;
function last(arr) {
    if (arr.length === 0) {
        return undefined;
    }
    else {
        return arr[arr.length - 1];
    }
}
exports.last = last;
exports.default = {
    cross: cross,
    crossPlus: crossPlus,
    diff: diff,
    unique: unique,
    recursion: recursion,
    camelCase: camelCase,
    mergeObjArr: mergeObjArr,
    last: last,
};
