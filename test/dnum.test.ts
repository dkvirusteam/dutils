import { dnum } from '../src'

describe('测试数值常用方法', () => {
    test('测试加法 plus()', () => {
        // js 里 0.1 + 0.2 = 0.30000000000000004
        expect(dnum.plus(0.1, 0.2)).toBe(0.3)
    })

    test('测试减法 minus()', () => {
        // js 里 0.3 - 0.1 = 0.19999999999999998
        expect(dnum.minus(0.3, 0.1)).toBe(0.2)
    })

    test('测试乘法 multiply()', () => {
        // js 里 0.1 * 0.2 = 0.020000000000000004
        expect(dnum.multiply(0.1, 0.2)).toBe(0.02)
    })

    test('测试除法 divide()', () => {
        // js 里 0.3 / 0.1 = 2.9999999999999996
        expect(dnum.divide(0.3, 0.1)).toBe(3)
    })

    test('测试字节格式化 formatByte()', () => {
        expect(dnum.formatByte('123a')).toBe('')
        expect(dnum.formatByte(502)).toBe('502B')
        expect(dnum.formatByte('502')).toBe('502B')
        expect(dnum.formatByte(1024)).toBe('1KB')
        expect(dnum.formatByte(1024 * 1024 * 3)).toBe('3MB')
        expect(dnum.formatByte(1024 * 1024 * 1024 * 4)).toBe('4GB')
    })

    test('测试数字格式化，每三位数字加逗号隔开 formatNumberByComma()', () => {
        expect(dnum.formatNumberByComma('123a')).toBe('123a')
        expect(dnum.formatNumberByComma('123456')).toBe('123,456')
        expect(dnum.formatNumberByComma('1234567')).toBe('1,234,567')
        expect(dnum.formatNumberByComma('1234.5678')).toBe('1,234.5678')
        expect(dnum.formatNumberByComma(false)).toBe('')
        expect(dnum.formatNumberByComma(null)).toBe('')
        expect(dnum.formatNumberByComma(undefined)).toBe('')
        expect(dnum.formatNumberByComma({})).toBe('')
        expect(dnum.formatNumberByComma([])).toBe('')
    })

    test('测试数字格式化，用 K 表示一千 formatNumberByKilo()', () => {
        expect(dnum.formatNumberByKilo('123a')).toBe('123a')
        expect(dnum.formatNumberByKilo('123456')).toBe('123.456K')
        expect(dnum.formatNumberByKilo('123456', {lowerCase: true})).toBe('123.456k')
        expect(dnum.formatNumberByKilo('123456', {decimal: 1})).toBe('123.5K')
        expect(dnum.formatNumberByKilo(false)).toBe('')
        expect(dnum.formatNumberByKilo(null)).toBe('')
        expect(dnum.formatNumberByKilo(undefined)).toBe('')
        expect(dnum.formatNumberByKilo({})).toBe('')
        expect(dnum.formatNumberByKilo([])).toBe('')
    })

    test('测试是否是数值 isNumber()', () => {
        expect(dnum.isNumber('a')).toBeFalsy()
        expect(dnum.isNumber('1')).toBeFalsy()
        expect(dnum.isNumber(1)).toBeTruthy()
        expect(dnum.isNumber(1.1)).toBeTruthy()
    })

    test('测试是否是整数 isInt()', () => {
        expect(dnum.isInt('a')).toBeFalsy()
        expect(dnum.isInt('1')).toBeFalsy()
        expect(dnum.isInt(1)).toBeTruthy()
        expect(dnum.isInt(1.1)).toBeFalsy()
    })

    test('测试是否是小数 isFloat()', () => {
        expect(dnum.isFloat('a')).toBeFalsy()
        expect(dnum.isFloat('1')).toBeFalsy()
        expect(dnum.isFloat(1)).toBeFalsy()
        expect(dnum.isFloat(1.1)).toBeTruthy()
    })
})