import { curlToJson } from '../src/dcurl'

describe('测试 curl 方法', () => {

    describe('测试 curlToJson()', () => {
        it('测试 curlToJson()', () => {
            const curlStr = `curl 'https://api.norna.ai/color_material_category_assortment?is_lookbook=false&selected_property=category' \
            -H 'content-type: application/x-www-form-urlencoded' \
            -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36' \
            --data-raw '{"is_lookbook":false, "age": 21}' \
            --compressed`
            const jsonObj = {
                url: 'https://api.norna.ai/color_material_category_assortment',
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
                  'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/100.0.4896.60 Safari/537.36',
                  'Accept-Encoding': 'deflate, gzip'
                },
                params: { is_lookbook: 'false', selected_property: 'category' },
                data: { is_lookbook: false, age: 21 }
            }
            expect(curlToJson(curlStr)).toEqual(jsonObj)
        })

        it('测试 -A 参数', () => {
            const curlStr = 'curl -A "Chrome" http://httpbin.org/get'
            const jsonObj = {
                url: 'http://httpbin.org/get',
                method: 'GET',
                headers: {
                    'User-Agent': 'Chrome',
                },
                params: {},
                data: {},
            }
            expect(curlToJson(curlStr)).toEqual(jsonObj)
        })
    })
})


