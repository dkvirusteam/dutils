import { ddate } from '../src'

describe('测试日期常用方法', () => {
    it('测试参数为空', () => {
        expect(ddate.formatDate().hms).toBeUndefined()
    })

    it('测试参数为时间字符串', () => {
        const now = 'Sun Apr 11 2021 18:28:10 GMT+0800 (中国标准时间)'
        expect(ddate.formatDate(now).ymd).toBe('2021-04-11')
    })

    it('测试参数为时间字符串2', () => {
        const now = '2021-04-11'
        expect(ddate.formatDate(now).ymd).toBe('2021-04-11')
    })

    it('测试分隔符', () => {
        const now = '2021-04-11'
        expect(ddate.formatDate(now, { separator: '/' }).ymd).toBe('2021/04/11')
    })
})