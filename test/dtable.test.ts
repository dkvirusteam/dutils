import { dtable } from '../src'

describe('测试表格常用方法', () => {
    it('测试增加序号列，第一页', () => {
        const dataSource = [{ name: '刘备' }, { name: '张飞' }]
        const expectDataSource = [{ index: 1, name: '刘备' }, { index: 2, name: '张飞' }]
        expect(dtable.addIndexColumn(dataSource)).toEqual(expectDataSource)
        expect(dtable.addIndexColumn(dataSource, 1, 10)).toEqual(expectDataSource)
    })

    it('测试增加序号列，第二页', () => {
        const dataSource = [{ name: '刘备' }, { name: '张飞' }]
        const expectDataSource = [{ index: 11, name: '刘备' }, { index: 12, name: '张飞' }]
        expect(dtable.addIndexColumn(dataSource, 2, 10)).toEqual(expectDataSource)
    })
})