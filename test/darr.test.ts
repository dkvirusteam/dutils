import { darr } from '../src'

describe('测试数组方法', () => {
    describe('测试两个数组求交集 cross()', () => {
        test('测试两个数组求交集 cross()', () => {
            expect(darr.cross([1, 2], [3, 1])).toEqual([1])
            expect(darr.cross([1, 2, 3], [1])).toEqual([1])
            expect(darr.cross([1], [1, 2, 3])).toEqual([1])
        })

        test('测试对象数组求交集 cross()', () => {
            expect(darr.cross([{'name': 'a1'}], [{'name': 'a2'}], 'name')).toEqual([])
            expect(darr.cross([{'name': 'a1'}, {'name': 'a2'}], [{'name': 'a2'}], 'name')).toEqual([{'name': 'a2'}])
            expect(darr.cross([{'name': 'a1'}], [{'name': 'a1'}, {'name': 'a2'}], 'name')).toEqual([{'name': 'a1'}])
        })
    })

    describe('测试数组求交集 crossPlus()', () => {
        test('基础数据类型数组求交集 crossPlus()', () => {
            const result = darr.crossPlus([
                [1,2,3],
                [2,3,4],
                [2,5,8],
            ])
            expect(result).toEqual([2])
        })

        test('对象数组求交集 crossPlus()', () => {
            const result = darr.crossPlus(
                [
                    [{'name': 'a1'}],
                    [{'name': 'a1'}, {'name': 'a2'}],
                    [{'name': 'a1'}, {'name': 'a3'}],
                ],
                'name'
            )
            expect(result).toEqual([{'name': 'a1'}])
        })
    })

    describe('测试两个数组求差集 diff()', () => {
        test('测试两个数组求差集 diff()', () => {
            expect(darr.diff([2, 1], [1, 3])).toEqual([2, 3])

        })

        test('测试两个数组求差集，不改变原数组 diff()', () => {
            const arr1 = [2, 1]
            const arr2 = [1, 3]
            darr.diff(arr1, arr2)
            expect(arr2).toEqual([1, 3])
        })
    })

    test('测试数组去重', () => {
        expect(darr.unique([1, 2, 1])).toEqual([1, 2])
    })

    describe('测试递归改变数据内数据结构 recursion()', () => {
        test('测试有 children 字段，模式为 append', () => {
            const arr = [{ id: '1id', name: '1name', children: [{ id: '11id', name: '11name' }] }]
            const expectArr = [{ id: '1id', name: '1name', key: '1id', title: '1name', children: [{ id: '11id', name: '11name', key: '11id', title: '11name' }] }]
            expect(darr.recursion(arr, { key: 'id', title: 'name' })).toEqual(expectArr)
        })

        test('测试没有 children 字段， 模式为 append', () => {
            const arr = [{ id: '1id', name: '1name', routes: [{ id: '11id', name: '11name' }] }]
            const expectArr = [{ id: '1id', name: '1name', key: '1id', title: '1name', routes: [{ id: '11id', name: '11name', key: '11id', title: '11name' }], children: [{ id: '11id', name: '11name', key: '11id', title: '11name' }] }]
            expect(darr.recursion(arr, { key: 'id', title: 'name', children: 'routes' })).toEqual(expectArr)
        })

        test('测试有 children 字段，模式为 new', () => {
            const arr = [{ id: '1id', name: '1name', children: [{ id: '11id', name: '11name' }] }]
            const expectArr = [{ key: '1id', title: '1name', children: [{ key: '11id', title: '11name' }] }]
            expect(darr.recursion(arr, { key: 'id', title: 'name' }, 'new')).toEqual(expectArr)
        })

        test('测试没有 children 字段， 模式为 new', () => {
            const arr = [{ id: '1id', name: '1name', routes: [{ id: '11id', name: '11name' }] }]
            const expectArr = [{ key: '1id', title: '1name', children: [{ key: '11id', title: '11name' }] }]
            expect(darr.recursion(arr, { key: 'id', title: 'name', children: 'routes' }, 'new')).toEqual(expectArr)
        })

        test('测试有 children 字段，第二个参数是个回调函数', () => {
            const arr = [{ id: '1id', hasChildren: true, children: [{ id: '11id', hasChildren: false }] }]
            const expectArr = [{ id: '1id', isLeaf: false, children: [{ id: '11id', isLeaf: true }] }]
            expect(darr.recursion(arr, (item: any) => {
                item.isLeaf = !item.hasChildren
                delete item.hasChildren
            })).toEqual(expectArr)
        })

        test('测试没有 children 字段，第二个参数是个回调函数，回调函数内为 children 属性赋值', () => {
            const arr = [{ id: '1id', hasChildren: true, descendant: [{ id: '11id', hasChildren: false }] }]
            const expectArr = [{ id: '1id', isLeaf: false, children: [{ id: '11id', isLeaf: true }] }]
            expect(darr.recursion(arr, (item: any) => {
                item.children = item.descendant
                item.isLeaf = !item.hasChildren
                delete item.hasChildren
                delete item.descendant
            })).toEqual(expectArr)

        })

        test('测试没有 children 字段，第二个参数是个回调函数，', () => {
            const arr = [{ id: '1id', hasChildren: true, descendant: [{ id: '11id', hasChildren: false }] }]
            const expectArr = [{ id: '1id', isLeaf: false, descendant: [{ id: '11id', hasChildren: false }] }]
            expect(darr.recursion(arr, (item: any) => {
                item.isLeaf = !item.hasChildren
                delete item.hasChildren
            })).toEqual(expectArr)
        })
    })

    describe('测试数组去重方法 unique()', () => {
        test('一般数据结构数组测试数组去重方法', () => {
            const arr = [1, 2, 1, 3, 1]
            const expectArr = [1, 2, 3]
            expect(darr.unique(arr)).toEqual(expectArr)
        })

        test('对象数组测试数组去重方法', () => {
            const arr = [{ id: '1', name: 'dkvirus' }, { id: '2', name: 'dreamlin' }, { id: '3', name: 'dkvirus' }]
            const expectArr = [{ id: '1', name: 'dkvirus' }, { id: '2', name: 'dreamlin' }]
            expect(darr.unique(arr, ['name'])).toEqual(expectArr)
        })

        test('混合数据类型数组测试数组去重方法', () => {
            const arr = [1, 2, 1, { id: '1', name: 'dkvirus' }, { id: '2', name: 'dreamlin' }, { id: '3', name: 'dkvirus' }]
            const expectArr = [1, 2, { id: '1', name: 'dkvirus' }, { id: '2', name: 'dreamlin' }]
            expect(darr.unique(arr, ['name'])).toEqual(expectArr)
        })
    })

    describe('测试数组转驼峰方法 camelCase()', () => {
        test('测试转小驼峰', () => {
            expect(darr.camelCase(['system', 'user'])).toBe('systemUser')
            expect(darr.camelCase(['planDesign', 'projectRole'])).toBe('planDesignProjectRole')
        })

        test('测试转大驼峰', () => {
            expect(darr.camelCase(['system', 'user'], 'upper')).toBe('SystemUser')
            expect(darr.camelCase(['planDesign', 'projectRole'], 'upper')).toBe('PlanDesignProjectRole')
        })
    })

    describe('测试对象数组合并方法 mergeObjArr()', () => {
        test('对象数组长度一致，根据一个字段合并对象数组', () => {
            const arr1 = [{ id: 1, name: '张飞' }]
            const arr2 = [{ id: 1, age: 23 }]
            const expectArr = [{ id: 1, name: '张飞', age: 23 }]
            expect(darr.mergeObjArr(arr1, arr2, ['id'])).toEqual(expectArr)
        })

        test('对象数组长度一致，根据两个字段合并对象数组', () => {
            const arr1 = [{ id: 1, name: '张飞', age: 23 }]
            const arr2 = [{ id: 1, name: '张飞', sex: 'male' }]
            const expectArr = [{ id: 1, name: '张飞', age: 23, sex: 'male' }]
            expect(darr.mergeObjArr(arr1, arr2, ['id', 'name'])).toEqual(expectArr)
        })

        test('对象数组长度一致，第三个参数为对象', () => {
            const arr1 = [{ id: 1, name: '张飞', age: 23 }]
            const arr2 = [{ userId: 1, name: '张飞', sex: 'male' }]
            const expectArr = [{ id: 1, userId: 1, name: '张飞', age: 23, sex: 'male' }]
            expect(darr.mergeObjArr(arr1, arr2, { id: 'userId' })).toEqual(expectArr)
        })
    })

    describe('测试获取数组最后一项方法 last()', () => {
        it('数组长度为0', () => {
            expect(darr.last([])).toEqual(undefined)
        })

        it('数组长度为1', () => {
            expect(darr.last([{name: 'dk'}])).toEqual({name: 'dk'})
        })
    })
})