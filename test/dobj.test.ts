import { dobj } from '../src'

describe('测试对象相关方法', () => {

    describe('递归删除对象中属性值为空的属性 removeEmptyField()', () => {

        test('{a: 1, b: null, c: undefined, d: [], e: {}}', () => {
            const obj = { a: 1, b: null, c: undefined, d: [], e: {} }
            const expectObj = { a: 1 }
            expect(dobj.removeEmptyField(obj)).toEqual(expectObj)
        })

        test('嵌套对象字面量 {a: 1, b: { b1: {}, b2: [] }, c: {}}', () => {
            const obj = { a: 1, b: { b1: {}, b2: [] }, c: {} }
            const expectObj = { a: 1 }
            expect(dobj.removeEmptyField(obj)).toEqual(expectObj)
        })

        test('测试大数据量', () => {
            const obj = {
                "contractCode": "HT091009810",
                "contract_id": "551916329463463936",
                "importLevel": "2",
                "contractAttribute": "2",
                "contractVersion": null,
                "contractTextIsModel": null,
                "signTime": null,
                "contractSealTime": null,
                "projectNameContract": null,
                "projectManagement": null,
                "remark": null,
                "pricingMethod": null,
                "contractPriceTax": 0,
                "contractPriceNotTax": 0,
                "addedTax": 0,
                "outOfPriceExpenses": null,
                "contractAwardingMethod": null,
                "safetyAndCivilizationMeasuresPrice": 0,
                "domesticAndForeign": null,
                "natureOfTheProject": null,
                "natureOfTheProjectOther": null,
                "architecturalGroup": null,
                "contractNum": 0,
                "contractContent": null,
                "implementationProjectManagerId": null,
                "implementationProjectManagerName": null,
                "contractManager": null,
                "constructionUnitName": null,
                "professionalSubcontractorId": null,
                "professionalSubcontractorName": null,
                "legalRepresentativeOrEntrustedAgentId": null,
                "legalRepresentativeOrEntrustedAgentName": null,
                "professionalSubcontractingAgentsType": null,
                "professionalContractorId": null,
                "professionalContractorName": null,
                "professionalSubcontractorType": null,
                "contractScope": null,
                "contractScopeOther": null,
                "contractStartTime": null,
                "contractCompleteTime": null,
                "contractTimeLimit": null,
                "qualityStandard": null,
                "qualityStandardOther": null,
                "safeRequirement": null,
                "safeRequirementOther": null,
                "qualityStandardRequire": null,
                "contractAdjustmentFactors": null,
                "adjustmentScope": null,
                "adjustmentMethod": null,
                "contractGuarantee": null,
                "taxpayerQualification": null,
                "invoiceNum": null,
                "invoiceAccount": null,
                "invoiceBank": null,
                "invoiceAddr": null,
                "invoicePhone": null,
                "invoiceType": null,
                "invoiceDeducte": "0",
                "invoiceRate": null,
                "contractFunds": {
                    "fundsPayment": "按月进度支付",
                    "contractPrice": null,
                    "wagesWay": null,
                    "workloadRatio": null,
                    "mainStructure": null,
                    "pass": null,
                    "reportData": null,
                    "auditFirst": null,
                    "auditTwo": null,
                    "qualityMoney": null,
                    "goldBack": null,
                    "engineerChange": null,
                    "reductionRateAgreement": null,
                    "reductionRateInstruction": null
                },
            }
            const expectObj = {
                "contractCode": "HT091009810",
                "contract_id": "551916329463463936",
                "importLevel": "2",
                "contractAttribute": "2",
                "contractPriceTax": 0,
                "contractPriceNotTax": 0,
                "addedTax": 0,
                "safetyAndCivilizationMeasuresPrice": 0,
                "contractNum": 0,
                "invoiceDeducte": "0",
                "contractFunds": {
                    "fundsPayment": "按月进度支付",
                },
            }
            expect(dobj.removeEmptyField(obj)).toEqual(expectObj)
        })
    })

    test('测试是否为对象方法 isObject()', () => {
        expect(dobj.isObject(1)).toBeFalsy()
        expect(dobj.isObject({})).toBeTruthy()
    })

    test('测试是否为空对象 isEmptyObject()', () => {
        expect(dobj.isEmptyObject(1)).toBeFalsy()
        expect(dobj.isEmptyObject({ a: 1 })).toBeFalsy()
        expect(dobj.isEmptyObject({})).toBeTruthy()
    })

})