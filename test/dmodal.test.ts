import { dmodal } from '../src'

describe('测试模态框相关方法 dmodal', () => {
    test('测试获取模态框标题方法 getModalTitle()', () => {
        expect(dmodal.getModalTitle('create')).toBe('新建')
        expect(dmodal.getModalTitle('create', '用户')).toBe('新建用户')
    })
})