import { dreg } from '../src'

describe('测试常用正则表达式', () => {
    it('值为手机号', () => {
        expect(dreg.isPhone('18012344321')).toBeTruthy()
    })

    it('值不为手机号', () => {
        expect(dreg.isPhone('123')).toBeFalsy()
    })

    it('值为邮箱', () => {
        expect(dreg.isEmail('123@qq.com')).toBeTruthy()
    })

    it('值不为邮箱', () => {
        expect(dreg.isEmail('123')).toBeFalsy()
    })
})