import { dstr } from '../src'

describe('测试字符串常用方法', () => {
    describe('测试字符串模板替换方法 replace()', () => {
        test('测试模式为 {{}}，默认值', () => {
            const template = 'hello {{name}}, age is {{age}}, bye {{name}}!'
            const params = { name: 'dkvirus', age: 18 }
            const expectStr = 'hello dkvirus, age is 18, bye dkvirus!'
            expect(dstr.replace(template, params)).toBe(expectStr)
        })

        test('测试模式为 {}', () => {
            const template = 'hello {name}, age is {age}, bye {name}!'
            const params = { name: 'dkvirus', age: 18 }
            const expectStr = 'hello dkvirus, age is 18, bye dkvirus!'
            expect(dstr.replace(template, params, '{}')).toBe(expectStr)
        })
    })

    test('测试字符串超出长度显示省略号 ellipsis()', () => {
        const str = 'helloworld'
        expect(dstr.ellipsis(str, 5)).toBe('hello...')
        expect(dstr.ellipsis(str, 10)).toBe('helloworld')
    })

    test('测试是否是数值字符串 isNumberStr()', () => {
        expect(dstr.isNumberStr('a1')).toBeFalsy()
        expect(dstr.isNumberStr('1a')).toBeFalsy()
        expect(dstr.isNumberStr('1')).toBeTruthy()
        expect(dstr.isNumberStr('1.1')).toBeTruthy()
    })
})