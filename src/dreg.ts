const phoneRegex = /^1[3456789]\d{9}$/
const emailRegex = /^([a-zA-Z]|[0-9])(\w|\-)+@[a-zA-Z0-9]+\.([a-zA-Z]{2,4})$/

const test = (regex: RegExp, value: string) => {
    return regex.test(value)
}

export function isEmail(email: string): boolean {
    return test(emailRegex, email)
}

export function isPhone(phone: string): boolean {
    return test(phoneRegex, phone)
}

export function isUrl(url: string): boolean {
    return /^https?:\/\//.test(url) || /^localhost?:/.test(url) || /^127.0.0.1?:/.test(url)
}

export default {
    isEmail,
    isPhone,
    isUrl,
}