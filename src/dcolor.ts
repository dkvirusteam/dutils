
/**
 * RGB颜色转换为16进制
 * 
 * rgbToHex('RGB(23, 245, 56)')  // => '#34538b'
 */
 export function rgbToHex(r: number, g: number, b: number): string {
    const value = `RGB(${r}, ${g}, ${b})`
    //十六进制颜色值的正则表达式
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
    // 如果是rgb颜色表示
    if (/^(rgb|RGB)/.test(value)) {
        var aColor = value.replace(/(?:\(|\)|rgb|RGB)*/g, "").split(",")
        var strHex = "#"
        for (var i=0; i<aColor.length; i++) {
            var hex = Number(aColor[i]).toString(16);
            if (hex.length < 2) {
                hex = '0' + hex 
            }
            strHex += hex
        }
        if (strHex.length !== 7) {
            strHex = value 
        }
        return strHex
    } else if (reg.test(value)) {
        var aNum = value.replace(/#/,"").split("");
        if (aNum.length === 6) {
            return value 
        } else if(aNum.length === 3) {
            var numHex = "#"
            for (var i=0; i<aNum.length; i+=1) {
                numHex += (aNum[i] + aNum[i])
            }
            return numHex
        }
    }
    return value
}

/**
 * 十六进制转 rgb 颜色
 */
export function hexToRgb(value: string): string {
    var sColor = value.toLowerCase()
    //十六进制颜色值的正则表达式
    var reg = /^#([0-9a-fA-f]{3}|[0-9a-fA-f]{6})$/
    // 如果是16进制颜色
    if (sColor && reg.test(sColor)) {
        if (sColor.length === 4) {
            var sColorNew = "#"
            for (var i=1; i<4; i+=1) {
                sColorNew += sColor.slice(i, i+1).concat(sColor.slice(i, i+1))  
            }
            sColor = sColorNew
        }
        //处理六位的颜色值
        var sColorChange = []
        for (var i=1; i<7; i+=2) {
            sColorChange.push(parseInt("0x"+sColor.slice(i, i+2)))  
        }
        return "rgb(" + sColorChange.join(", ") + ")"
    }
    return sColor
}

export default {
    rgbToHex,
    hexToRgb,
}