/**
 * 加法
 */
export function plus(arg1: number, arg2: number): number {
    let r1, r2, m
    try {
        r1 = String(arg1).split('.')[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = String(arg2).split('.')[1].length
    } catch (e) {
        r2 = 0
    }
    m = Math.pow(10, Math.max(r1, r2))
    return (arg1 * m + arg2 * m) / m
}

/**
 * 减法
 */
export function minus(arg1: number, arg2: number): number {
    let r1, r2, m, n
    try {
        r1 = String(arg1).split('.')[1].length
    } catch (e) {
        r1 = 0
    }
    try {
        r2 = String(arg2).split('.')[1].length
    } catch (e) {
        r2 = 0
    }
    n = Math.max(r1, r2)
    m = Math.pow(10, n)
    return Number(((arg1 * m - arg2 * m) / m).toFixed(n))
}

/**
 * 乘法
 */
export function multiply(arg1: number, arg2: number): number {
    let m = 0,
        s1 = String(arg1),
        s2 = String(arg2)
    try {
        m += s1.split('.')[1].length
    } catch (e) { }
    try {
        m += s2.split('.')[1].length
    } catch (e) { }
    return Number(s1.replace('.', '')) * Number(s2.replace('.', '')) / Math.pow(10, m)
}

/**
 * 除法
 */
export function divide(arg1: number, arg2: number): number {
    let t1, t2, r1, r2
    try {
        t1 = String(arg1).split('.')[1].length
    } catch (e) {
        t1 = 0
    }
    try {
        t2 = String(arg2).split('.')[1].length
    } catch (e) {
        t2 = 0
    }
    r1 = Number(String(arg1).replace('.', ''))
    r2 = Number(String(arg2).replace('.', ''))
    return (r1 / r2) * Math.pow(10, t2 - t1)
}

/**
 * 格式化字节数
 * @returns 
 */
export function formatByte(byteSize: number | string, decimal: number = 2): string {
    byteSize = Number(byteSize)
    if (String(byteSize) === 'NaN') return ''
    if (0 == byteSize) return '0B'
    const times = 1024
    const unitArr = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
    const index = Math.floor(Math.log(byteSize) / Math.log(times))
    return parseFloat((byteSize / Math.pow(times, index)).toFixed(decimal)) + unitArr[index]
}

/**
 * 数值每三位用逗号隔开
 */
export function formatNumberByComma(value: any): string {
    const num = Number(value)
    if (typeof value !== 'string' && typeof value !== 'number') {
        return ''
    } else if (String(num) === 'NaN') {
        return String(value)
    } else if (isInt(num)) {
        // 整数
        return String(num).replace(/\B(?=(\d{3})+(?!\d))/g, ',')
    } else {
        // 小数
        // numArr[0] 整数   numArr[1] 小数
        const numArr = String(value).split('.')
        numArr[0] = String(numArr[0]).replace(/\B(?=(\d{3})+(?!\d))/g, ',')
        return numArr.join('.')
    }
}

/**
 * 用 K 格式化数字
 */
export function formatNumberByKilo(value: any, opt?: { decimal?: number, lowerCase?: boolean }): string {
    if (typeof value !== 'string' && typeof value !== 'number') {
        return ''
    }

    const num = Number(value)
    if (String(num) === 'NaN' || num < 1000) {
        return String(value);
    } else {
        const unit = opt?.lowerCase === true ? 'k' : 'K'
        const newNum = opt?.decimal ? (num / 1000).toFixed(opt?.decimal) : (num / 1000)
        return newNum + unit
    }
}

/**
 * 是否是数值
 */
export function isNumber(value: any): boolean {
    return typeof value === 'number'
}

/**
 * 是否是整数
 */
export function isInt(value: any): boolean {
    return typeof value === 'number' && !String(value).includes('.')
}

/**
 * 是否是小数
 */
export function isFloat(value: any): boolean {
    return typeof value === 'number' && String(value).includes('.')
}

export default {
    plus,
    minus,
    multiply,
    divide,
    formatByte,
    formatNumberByComma,
    formatNumberByKilo,
    isNumber,
    isInt,
    isFloat,
}