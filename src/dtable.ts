/**
 * 表格数据添加序号列
 */
export function addIndexColumn(tableData: any[], current = 1, size = 10) {
    if (Object.prototype.toString.call(tableData) !== "[object Array]") {
        throw new Error("参数类型不正确")
    }
    const startIndex = (Number(current) - 1) * Number(size) + 1
    return tableData.map((item, index) => {
        item.index = startIndex + index
        return item
    })
}

export default {
    addIndexColumn,
}