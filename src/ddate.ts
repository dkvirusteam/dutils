export function formatDate(dateStr?: string, opt?: {
    separator?: string;
}) {
    let year, month, day, week, hour, minute, second, millisecond, ymd, hms, ymdhms, ymdhmss
    if (!dateStr || String(new Date(dateStr)) === 'Invalid Date') {
        return { year, month, day, week, hour, minute, second, millisecond, ymd, hms, ymdhms, ymdhmss }
    }
    const separator = opt?.separator || '-'
    const d = new Date(dateStr)
    year = String(d.getFullYear())
    month = String(d.getMonth() + 1).padStart(2, '0')
    day = String(d.getDate()).padStart(2, '0')
    hour = String(d.getHours()).padStart(2, '0')
    minute = String(d.getMinutes()).padStart(2, '0')
    second = String(d.getSeconds()).padStart(2, '0')
    millisecond = String(d.getMilliseconds()).padStart(3, '0')
    const weeks = ['周日', '周一', '周二', '周三', '周四', '周五', '周六']
    week = weeks[d.getDay()]
    ymd = `${year}${separator}${String(month).padStart(2, '0')}${separator}${String(day).padStart(2, '0')}`
    hms = `${String(hour).padStart(2, '0')}:${String(minute).padStart(2, '0')}:${String(second).padStart(2, '0')}`
    ymdhms = `${ymd} ${hms}`
    ymdhmss = `${ymdhms}.${millisecond}`
    return { year, month, day, week, hour, minute, second, millisecond, ymd, hms, ymdhms, ymdhmss }
}

export default {
    formatDate,
}