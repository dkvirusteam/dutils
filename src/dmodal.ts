export function getModalTitle(operType: 'create' | 'modify' | 'check', moduleName?: string): string {
    const operTypeTexts = {
        create: '新建',
        modify: '编辑',
        check: '查看',
    }
    return `${operTypeTexts[operType] || ''}${moduleName || ''}`
}

export default {
    getModalTitle,
}