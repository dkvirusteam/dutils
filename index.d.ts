/* ********************************* 数组相关方法 **************************************** */
export {
    cross,
    crossPlus,
    diff,
    unique,
    recursion,
    camelCase,
    mergeObjArr,
    last,
}

/**
 * 求两个数组的交集，共同的部分  
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#aOHKG
 */
declare function cross(arr1: any[], arr2: any[], field?: string): any[];

/**
 * 求两个数组的交集，共同的部分，参数可以是个二维数组
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#aOHKG
 */
declare function crossPlus(twoDimensionalArr: any[][], field?: string): any[];

/**
 * 求两个数组的差集，不同的部分  
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#SwMlW
 */
declare function diff(arr1: any[], arr2: any[]): any[];

/**
 * 数组去重
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#W6Vm5
 */
declare function unique(arr: any[], fields?: string[]): any[];

/**
 * 递归处理数组数据结构
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#DBuue
 */
declare function recursion(data: any[], fieldsOrcb: ((item: any) => void) | Record<string, any>, mode: 'append' | 'new' = 'append'): any[];

/**
 * 数组转驼峰字符串
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#NdgmP
 */
declare function camelCase(arr: string[], mode?: 'lower' | 'upper'): string;

/**
 * 对象数组合并
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/tob480#na86W
 */
declare function mergeObjArr(arr1: any[] = [], arr2: any[] = [], fieldsArrOrObj: string[] | Record<string, any>): any[];

/**
 * 获取数组最后一项
 */
declare function last(arr: any): any;

/* ********************************* 客户端相关方法 **************************************** */
export {
    parseUrlParams,
    getUrlParams,
    downloadFile,
    setLocalStorage,
    getLocalStorage,
    setSessionStorage,
    getSessionStorage,
    fullscreen,
    exitFullscreen,
    isFullscreen,
    toggleFullscreen,
    getCookie,
    setCookie,
    removeCookie,
    hasCookie,
}

/**
 * 解析 url 参数字符串
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#6gdhr
 */
declare function parseUrlParams(params: string): Record<string, any>;

/**
 * 获取 url 参数对象
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#wbaNL
 */
declare function getUrlParams(): Record<string, any>;

/**
 * 下载文件
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#pGCQz
 */
declare function downloadFile(url: string, filename: string, cb?: () => void): void;

/**
 * 设置 localStorage 值，包括对对象和数组类型处理
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#lnWq6
 */
declare function setLocalStorage(key: string, value: any): void;

/**
 * 获取 localStorage 值，包括对对象和数组类型处理
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#eCthH
 */
declare function getLocalStorage(key: string, defaultValue?: any): any;

/**
 * 设置 sessionStorage 值，包括对对象和数组类型处理
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#uuH9c
 */
declare function setSessionStorage(key: string, value: any): void;

/**
 * 获取 sessionStorage 值，包括对对象和数组类型处理
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#uQZEp
 */
declare function getSessionStorage(key: string, defaultValue?: any): any;

/**
 * 全屏展示
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#FsCbd
 */
declare function fullscreen(): Promise<void>;

/**
 * 退出全屏展示
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#FsCbd
 */
declare function exitFullscreen(): Promise<void>;

/**
 * 判断是否全屏展示
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#FsCbd
 */
declare function isFullscreen(): boolean;

/**
 * 切换全屏/退出全屏
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#FsCbd
 */
declare function toggleFullscreen(): void;

/**
 * 获取 cookie
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#KnkMA
 */
declare function getCookie(key: string): string;

/**
 * 设置 cookie
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#KnkMA
 */
declare function setCookie(key: string, value: string, expires = 3600, path = '/', secure = false): boolean;

/**
 * 删除 cookie
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#KnkMA
 */
declare function removeCookie(key: string, path = '/', domain = window.location.hostname): boolean;

/**
 * 判断是否包含 cookie
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/gwom2v#KnkMA
 */
declare function hasCookie(key: string): boolean;

/* ********************************* 颜色相关方法 **************************************** */
export {
    rgbToHex,
    hexToRgb,
}

/**
 * rgb 颜色值转换为十六进制颜色值
 */
declare function rgbToHex(r: number, g: number, b: number): string;

/**
 * 十六进制颜色值转换为 rgb 颜色值 
 */
declare function hexToRgb(value: string): string;

/* ********************************* curl相关方法 **************************************** */
export {
    curlToJsonResult,
    curlToJson,
}

interface curlToJsonResult {
    url: string;
    method: string;
    headers: Record<string, string>;
    params: Record<string, any>;
    data: Record<string, any>;
}

/**
 * curl 字符串转 json 字符串
 */
declare function curlToJson(value: string): curlToJsonResult;

/* ********************************* 日期相关方法 **************************************** */
export {
    formatDate,
}

/**
 * 格式化日期
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/qpfln4#4nVpy 
 */
declare function formatDate(value: string, opt?: { separator?: string; }): { year: string, month: string, day: string, week: string, hour: string, minute: string, second: string, millisecond: string, ymd: string, hms: string, ymdhms: string, ymdhmss: string }

/* ********************************* 小程序相关方法 **************************************** */
export {
    formatRichText,
}

/**
 * 处理小程序富文本里的图片宽度自适应
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/ghf89m#Z4nRY
 */
declare function formatRichText(html: string): string;

/* ********************************* 模态框相关方法 **************************************** */
export {
    getModalTitle,
}

/**
 * 获取模态框标题 
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/wck4mg#ucA6g
 */
declare function getModalTitle(operType: 'create' | 'modify' | 'check', moduleName?: string): string;

/* ********************************* 数值相关方法 **************************************** */
export {
    plus,
    minus,
    multiply,
    divide,
    formatByte,
    formatNumberByComma,
    formatNumberByKilo,
    isNumber,
    isInt,
    isFloat,
}

/**
 * 加法
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#zY9GQ
 */
declare function plus(arg1: number, arg2: number): number;

/**
 * 减法
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#2Dqud 
 */
declare function minus(arg1: number, arg2: number): number;

/**
 * 乘法
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#kyPpL
 */
declare function multiply(arg1: number, arg2: number): number;

/**
 * 除法
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#AOwhk 
 */
declare function divide(arg1: number, arg2: number): number;

/**
 * 格式化字节
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#B5yW5 
 */
declare function formatByte(byteSize: number | string, decimal?: number): string;

/**
 * 每三位数字用逗号隔开
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#gk8O0
 */
declare function formatNumberByComma(value: any): string;

/**
 * 用 K 格式化数值
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#sPWxC
 */
declare function formatNumberByKilo(value: any, opt?: { decimal?: number, lowerCase?: boolean }): string;

/**
 * 是否是数值
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#C2cHn
 */
declare function isNumber(value: any): boolean;

/**
 * 是否是整数
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#ZPrYC
 */
declare function isInt(value: any): boolean;

/**
 * 是否是小数
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/paazuy#XM8kR
 */
declare function isFloat(value: any): boolean;

/* ********************************* 对象相关方法 **************************************** */
export {
    removeEmptyField,
    isObject,
    isEmptyObject,
}

/**
 * 递归删除对象中属性值为空的属性，第二个参数用于定义空的类型
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/hrxflh#Mu3lB
 */
declare function removeEmptyField(obj: Record<string, any>, mode?: Array<'[]' | '{}' | 'null' | 'undefined' | '' | '0' | 'false'>): any;

/**
 * 判断是否是对象字面量
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/hrxflh#mIozf
 */
declare function isObject(value: any): boolean;

/**
 * 判断是否为空对象字面量，即 {}
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/hrxflh#5Pfdj
 */
declare function isEmptyObject(value: any): boolean;

/* ********************************* 正则相关方法 **************************************** */
export {
    isEmail,
    isPhone,
    isUrl,
}

/**
 * 校验是否是邮箱格式
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/ywuguc#efNmK
 */
declare function isEmail(email: string): boolean;

/**
 * 校验是否是手机格式
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/ywuguc#EOzop
 */
declare function isPhone(phone: string): boolean;

/**
 * 校验是否时网址
 */
declare function isUrl(url: string): boolean;

/* ********************************* 字符串相关方法 **************************************** */
export {
    replace,
    ellipsis,
    geneRandomStr,
    uuid,
    isNumberStr,
}

/**
 * 模板字符串替换
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/unud7k#xN6qV
 */
declare function replace(content: string, params: Record<string, any>, mode?: '{{}}' | '{}'): string;

/**
 * 字符串超出长度显示省略号
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/unud7k#Xuitu
 */
declare function ellipsis(content: string, length?: number): string;

/**
 * 生成随机字符串
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/unud7k#rUrKS
 */
declare function geneRandomStr(length: number = 6, characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'): string;

/**
 * 生成 uuid
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/unud7k#E7bxo
 */
declare function uuid(): string;

/**
 * 是否是数值字符串 
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/unud7k#G24sB
 */
declare function isNumberStr(value: string): boolean;

/* ********************************* 表格相关方法 **************************************** */
export {
    addIndexColumn,
}

/**
 * 表格增加序号列
 * 
 * document: https://www.yuque.com/teamdkvirus/frontend/hbm60v#Ra8GG
 */
declare function addIndexColumn(tableData: any[], current?: number, size?: number): any[];

/* ********************************* 兼容老版本类型声明 **************************************** */
/* ********************************* 兼容老版本类型声明 **************************************** */
/* ********************************* 兼容老版本类型声明 **************************************** */
export {
    darr,
    dclient,
    dcolor,
    dcurl,
    ddate,
    dminiapp,
    dnum,
    dreg,
    dstr,
    dtable,
}

declare namespace darr {
    function cross(arr1: any[], arr2: any[]): any[];
    function crossPlus(twoDimensionalArr: any[][], field?: string): any[];
    function diff(arr1: any[], arr2: any[]): any[];
    function unique(arr: any[], fields?: string[]): any[];
    function recursion(data: any[], fields: Record<string, any>, mode?: 'append' | 'new'): any[];
    function camelCase(arr: string[], mode?: 'lower' | 'upper'): string;
    function mergeObjArr(arr1: any[] = [], arr2: any[] = [], fields: string[] = []): any[];
    function last(arr: any[]): any;
}

declare namespace dclient {
    function parseUrlParams(params: string): Record<string, any>;
    function getUrlParams(): Record<string, any>;
    function downloadFile(url: string, filename: string): void;
    function setLocalStorage(key: string, value: any): void;
    function getLocalStorage(key: string, defaultValue?: any): any;
    function setSessionStorage(key: string, value: any): void;
    function getSessionStorage(key: string, defaultValue?: any): any;
}

declare namespace dcolor {
    function rgbToHex(r: number, g: number, b: number): string;
    function hexToRgb(value: string): string;
}

declare namespace dcurl {
    function curlToJson(value: string): curlToJsonResult;
}

declare namespace ddate {
    function formatDate(value: string): { year: string, month: string, day: string, week: string, hour: string, minute: string, second: string, millisecond: string, ymd: string, hms: string, ymdhms: string, ymdhmss: string }
}

declare namespace dminiapp {
    function formatRichText(html: string): string;
}

declare namespace dnum {
    function plus(arg1: number, arg2: number): number;
    function minus(arg1: number, arg2: number): number;
    function multiply(arg1: number, arg2: number): number;
    function divide(arg1: number, arg2: number): number;
    function formatByte(byteSize: number | string, decimal?: number): string;
    function formatNumberByComma(value: any): string;
    function formatNumberByKilo(value: any, opt?: { decimal?: number, lowerCase?: boolean }): string;
    function isNumber(value: any): boolean;
    function isInt(value: any): boolean;
    function isFloat(value: any): boolean;
}

declare namespace dreg {
    function isEmail(email: string): boolean;
    function isPhone(phone: string): boolean;
    function isUrl(url: string): boolean;
}

declare namespace dstr {
    function replace(content: string, params: Record<string, any>, mode?: '{{}}' | '{}'): string;
    function ellipsis(content: string, length?: number): string;
    function geneRandomStr(length: number = 6, characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'): string;
    function uuid(): string;
    function isNumberStr(value: string): boolean;
}

declare namespace dtable {
    function addIndexColumn(tableData: any[], current?: number, size?: number): any[];
}