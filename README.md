## dutils

### 安装

``` bash
$ yarn add @dkvirus/dutils
```

### 文档

[文档](https://www.yuque.com/teamdkvirus/frontend/rko8es)

### 打包

typescript 源码在 src 目录下；

打包命令会生成 lib 目录，该目录下存放 es5 源码。

``` bash
$ yarn build
```

### 测试

```
$ yarn test
```